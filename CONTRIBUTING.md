Les contributions externes sont les bienvenues ! N’hésitez pas à faire des *pull request* ou ou à ouvrir un ticket.

Si vous ne souhaitez pas ouvrir un compte sur la plateforme, vous pouvez m’écrire un courriel directement.

Je suis également preneur de retours : facilité d’utilisation, cas d’usage, durée de retranscription, etc.
