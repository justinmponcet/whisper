FROM python:3.12-slim

WORKDIR /app

# Installer les outils nécessaires
RUN apt-get update && apt-get install -y \
    build-essential \
    libffi-dev \
    libssl-dev \
    python3-dev \
    && apt-get clean

COPY . .

RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt

RUN chmod +x run.sh
EXPOSE 8000

CMD ["./run.sh"]
