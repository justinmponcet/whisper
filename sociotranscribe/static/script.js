const audio = document.getElementById('audioPlayer');
const fileInput = document.getElementById('fileInput');
const audioContainer = document.getElementById('audioContainer');
const speedDisplay = document.getElementById('speedDisplay');
const speedSlider = document.getElementById('speedSlider');
const editor = document.getElementById('editor');
const wordCountDisplay = document.getElementById('wordCount');
const customTimeInput = document.getElementById('customTime');
const playPauseButton = document.getElementById('playPauseButton');
let playbackRate = 1.0;

window.onload = function() {
    const savedEditorContent = localStorage.getItem('editorContent');
    const savedPlaybackRate = parseFloat(localStorage.getItem('playbackRate')) || 1.0;

    if (savedEditorContent) {
        editor.innerHTML = savedEditorContent;
        updateWordCount();
    }

    playbackRate = savedPlaybackRate;
    speedSlider.value = playbackRate;
    audio.playbackRate = playbackRate;
    updateSpeedDisplay();
    
    const params = new URLSearchParams(window.location.search);
    const audioUrl = params.get('audio');
    const subtitleUrl = params.get('subtitle');

    if (audioUrl) {
        loadAudioFromUrl(audioUrl);
    }
if (subtitleUrl) {
    console.log("URL des sous-titres:", subtitleUrl);

    fetch(subtitleUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Erreur de chargement des sous-titres, code HTTP: ${response.status}`);
            }
            return response.text();  // Retourne le texte du fichier
        })
        .then(data => {
            const subtitles = parseSRT(data);
            displaySubtitles(subtitles);
            console.log(subtitles);  // Affiche les sous-titres dans la console
        })
        .catch(error => {
            console.error('Erreur lors du chargement des sous-titres:', error);
        });
}
};

function loadAudioFromUrl(url) {
    audio.src = url;
    audio.play();
    audioContainer.style.display = 'flex';
    fileInput.parentElement.style.display = 'none';
}

fileInput.addEventListener('change', (event) => {
    const file = event.target.files[0];
    if (file) {
        const fileURL = URL.createObjectURL(file);
        loadAudioFromUrl(fileURL);
        localStorage.setItem('audioFileURL', fileURL);
    }
});

function togglePlayPause() {
    if (audio.paused) {
        audio.play();
        playPauseButton.innerHTML = '<svg class="icon icon-pause"><use xlink:href="#icon-pause"></use></svg>';
    } else {
        audio.pause();
        audio.currentTime -= 1;
        playPauseButton.innerHTML = '<svg class="icon icon-play"><use xlink:href="#icon-play"></use></svg>';
    }
}

function rewind() {
    audio.currentTime -= 5;
}

function fastForward() {
    audio.currentTime += 5;
}

function updateSpeed() {
    playbackRate = parseFloat(speedSlider.value);
    audio.playbackRate = playbackRate;
    localStorage.setItem('playbackRate', playbackRate);
    updateSpeedDisplay();
}

function updateSpeedDisplay() {
    speedDisplay.textContent = `${playbackRate.toFixed(1)}x`;
}

function execCmd(command) {
    document.execCommand(command, false, null);
    saveEditorContent();
}

function insertTimestamp() {
    if (audio.src) {
        const currentTime = audio.currentTime;
        const minutes = Math.floor(currentTime / 60);
        const seconds = Math.floor(currentTime % 60);
        const timestampText = `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
        const timestampLink = `<a href="#" class="timestamp" onclick="seekTo(${currentTime})">${timestampText}</a> `;

        document.execCommand('insertHTML', false, timestampLink);
        saveEditorContent();
    } else {
        alert('Veuillez charger un fichier audio avant d\'insérer un timestamp.');
    }
}

function seekTo(time) {
    audio.currentTime = time;
    audio.play();
}

function updateWordCount() {
    const text = editor.textContent.trim();
    const wordCount = text ? text.split(/\s+/).length : 0;
    wordCountDisplay.textContent = `Nombre de mots: ${wordCount}`;
    saveEditorContent();
}

function saveEditorContent() {
    localStorage.setItem('editorContent', editor.innerHTML);
}

function updateCustomTimeInput() {
    const currentTime = audio.currentTime;
    const minutes = Math.floor(currentTime / 60);
    const seconds = Math.floor(currentTime % 60);
    customTimeInput.value = `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
}

function jumpToCustomTime() {
    const timeString = customTimeInput.value;
    const [minutes, seconds] = timeString.split(':').map(Number);
    if (!isNaN(minutes) && !isNaN(seconds)) {
        const timeInSeconds = (minutes || 0) * 60 + (seconds || 0);
        if (timeInSeconds >= 0 && timeInSeconds <= audio.duration) {
            audio.currentTime = timeInSeconds;
            audio.play();
        } else {
            alert('Le temps spécifié est hors limites.');
        }
    } else {
        alert('Format de temps invalide. Utilisez MM:SS.');
    }
}

function toggleVolume() {
    const audioPlayer = document.getElementById('audioPlayer');
    const volumeSlider = document.getElementById('volumeSlider');
    const volumeIcon = document.getElementById('volumeIcon');

    if (audioPlayer.volume === 0) {
        audioPlayer.volume = volumeSlider.value;
        volumeSlider.value = audioPlayer.volume;
        volumeIcon.querySelector('use').setAttribute('xlink:href', '#icon-volume-low');
    } else {
        audioPlayer.volume = 0;
        volumeSlider.value = 0;
        volumeIcon.querySelector('use').setAttribute('xlink:href', '#icon-volume-mute');
    }
}

function updateVolume() {
    const audioPlayer = document.getElementById('audioPlayer');
    const volumeSlider = document.getElementById('volumeSlider');
    audioPlayer.volume = volumeSlider.value;
}

document.getElementById('volumeSlider').addEventListener('input', updateVolume);


function clearLocalStorage() {
    localStorage.removeItem('audioFileURL');
    localStorage.removeItem('editorContent');
    localStorage.removeItem('playbackRate');
    fileInput.value = '';
    audio.src = '';
    audioContainer.style.display = 'none';
    fileInput.parentElement.style.display = 'block';
    editor.innerHTML = '';
    wordCountDisplay.textContent = 'Nombre de mots: 0';
    speedSlider.value = 1.0;
    audio.playbackRate = 1.0;
    speedDisplay.textContent = '1.0x';
}

speedSlider.addEventListener('input', updateSpeed);

document.addEventListener('keydown', (event) => {
    if (event.ctrlKey) {
        switch (event.key) {
            case 'b':
                event.preventDefault();
                execCmd('bold');
                break;
            case 'i':
                event.preventDefault();
                execCmd('italic');
                break;
              case 'u':
                event.preventDefault();
                execCmd('underline');
                break;
            case 'j':
                event.preventDefault();
                insertTimestamp();
                break;
            case 'k':
                event.preventDefault();
                jumpToCustomTime();
                break;
            case 's':
                event.preventDefault();
                exportTranscription();
                break;
        }
    } else if (event.code === 'Escape') {
        event.preventDefault();
        togglePlayPause();
    } else if (event.code === 'F1') {
        event.preventDefault();
        rewind();
    } else if (event.code === 'F2') {
        event.preventDefault();
        fastForward();
    }
});

function exportTranscription() {
    const editorContent = editor.innerHTML;
    const blob = new Blob([editorContent], { type: 'text/html' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'transcription.html';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
}
function exportToSRT() {
    const subtitles = document.querySelectorAll('.subtitle');
    if (subtitles.length === 0) {
        alert("Aucun sous-titre à exporter.");
        return;
    }

    let srtContent = '';

    subtitles.forEach((subtitle, index) => {
        const speaker = subtitle.querySelector('.speaker').textContent.trim();
        const start = parseFloat(subtitle.getAttribute('data-start'));
        const end = parseFloat(subtitle.getAttribute('data-end'));
        const text = subtitle.textContent
            .replace(speaker, '') // Supprimer le locuteur pour éviter de l'afficher en double
            .trim();

        // Formater le SRT
        srtContent += `${index + 1}\n`;
        srtContent += `${formatSRTTime(start)} --> ${formatSRTTime(end)}\n`;
        srtContent += `${speaker}: ${text}\n\n`;
    });

    // Générer et télécharger le fichier SRT
    const blob = new Blob([srtContent], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'transcription.srt';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
}

// Fonction pour convertir le temps en format SRT
function formatSRTTime(seconds) {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);
    const secs = Math.floor(seconds % 60);
    const milliseconds = Math.floor((seconds % 1) * 1000);
    return `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(secs).padStart(2, '0')},${String(milliseconds).padStart(3, '0')}`;
}

// Update volume based on slider value
const volumeSlider = document.getElementById('volumeSlider');
volumeSlider.addEventListener('input', () => {
    audio.volume = volumeSlider.value;
});

// Update current time and duration display
audio.addEventListener('timeupdate', () => {
    const percentage = (audio.currentTime / audio.duration) * 100;
    progressBar.style.width = `${percentage}%`;
    const currentTime = formatTime(audio.currentTime);
    const duration = formatTime(audio.duration);
    document.getElementById('currentTime').textContent = currentTime;
    document.getElementById('duration').textContent = duration;
});

function formatTime(seconds) {
    const minutes = Math.floor(seconds / 60);
    seconds = Math.floor(seconds % 60);
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
}

const progressBarContainer = document.querySelector('.progress-container');
const progressBar = document.getElementById('progressBar');


progressBarContainer.addEventListener('click', (event) => {
    const rect = progressBarContainer.getBoundingClientRect();
    const offsetX = event.clientX - rect.left;
    const width = rect.width;
    const percentage = offsetX / width;
    const newTime = percentage * audio.duration;
    audio.currentTime = newTime;
});


const subtitleInput = document.getElementById('subtitleInput');
subtitleInput.addEventListener('change', (event) => {
    const file = event.target.files[0];
    if (file) {
        const reader = new FileReader();
        reader.onload = (e) => {
            const subtitles = parseSRT(e.target.result);
            displaySubtitles(subtitles);
        };
        reader.readAsText(file);
    }
});

function parseSRT(data) {
    const regex = /(\d+)\n(\d{2}:\d{2}:\d{2},\d{3}) --> (\d{2}:\d{2}:\d{2},\d{3})\n([^:\n]*):? ([\s\S]*?)(?=\n\n|\n$)/g;
    const subtitles = [];
    let match;
    while ((match = regex.exec(data)) !== null) {
        subtitles.push({
            index: parseInt(match[1], 10),
            start: parseTime(match[2]),
            end: parseTime(match[3]),
            speaker: match[4].trim() || "Unknown",
            text: match[5].trim(),
        });
    }
    return subtitles;
}


function parseTime(timeString) {
    const [hours, minutes, seconds] = timeString.split(':');
    const [sec, ms] = seconds.split(',');
    return (
        parseInt(hours, 10) * 3600 +
        parseInt(minutes, 10) * 60 +
        parseInt(sec, 10) +
        parseInt(ms, 10) / 1000
    );
}

function displaySubtitles(subtitles) {
    const editor = document.getElementById('editor');
    editor.innerHTML = ''; // Réinitialiser le contenu de l'éditeur

    subtitles.forEach((subtitle) => {
        const subtitleElement = document.createElement('p');
        subtitleElement.classList.add('subtitle');
        subtitleElement.setAttribute('data-start', subtitle.start);
        subtitleElement.setAttribute('data-end', subtitle.end);
        subtitleElement.innerHTML = `
           [<span class="timestamp" onclick="seekTo(${subtitle.start})">${formatTime(subtitle.start)}</span>]
           [<span class="speaker">${subtitle.speaker}</span>]
            ${subtitle.text}
        `;
        editor.appendChild(subtitleElement);
    });
}


audio.addEventListener('timeupdate', () => {
    const currentTime = audio.currentTime;
    const subtitles = document.querySelectorAll('.subtitle');
    subtitles.forEach((subtitle) => {
        const start = parseFloat(subtitle.getAttribute('data-start'));
        const end = parseFloat(subtitle.getAttribute('data-end'));
        if (currentTime >= start && currentTime <= end) {
            subtitle.classList.add('active');
        } else {
            subtitle.classList.remove('active');
        }
    });
});


function replaceTextInEditor(findText, replaceText) {
    if (!findText.trim()) {
        alert("Le texte à trouver ne peut pas être vide.");
        return;
    }

    const subtitles = document.querySelectorAll('.subtitle');
    let changesMade = false;

    // Parcourir chaque sous-titre
    subtitles.forEach((subtitle) => {
        const speakerElement = subtitle.querySelector('.speaker');
        const textElement = subtitle;

        // Remplacement dans les noms de locuteur
        if (speakerElement && speakerElement.textContent.includes(findText)) {
            speakerElement.textContent = speakerElement.textContent.replace(new RegExp(findText, 'g'), replaceText);
            changesMade = true;
        }

        // Remplacement dans le texte des sous-titres
        if (textElement && textElement.textContent.includes(findText)) {
            textElement.innerHTML = textElement.innerHTML.replace(new RegExp(findText, 'g'), replaceText);
            changesMade = true;
        }
    });

    if (changesMade) {
        alert("Remplacement effectué !");
    } else {
        alert("Aucune correspondance trouvée pour le texte spécifié.");
    }
}

function handleReplaceText() {
    const findText = document.getElementById('findText').value.trim();
    const replaceText = document.getElementById('replaceText').value.trim();

    if (!findText) {
        alert("Veuillez saisir le texte à trouver.");
        return;
    }

    replaceTextInEditor(findText, replaceText);
}
function groupSubtitlesBySpeaker() {
    const subtitles = document.querySelectorAll('.subtitle');
    if (subtitles.length === 0) {
        alert("Aucun sous-titre à regrouper.");
        return;
    }

    let groupedSubtitles = [];
    let currentGroup = null;

    subtitles.forEach((subtitle) => {
        const speakerElement = subtitle.querySelector('.speaker');
        const textElement = subtitle.querySelector('.text') || subtitle;
        const start = parseFloat(subtitle.getAttribute('data-start'));
        const end = parseFloat(subtitle.getAttribute('data-end'));

        // Vérifier que les éléments nécessaires existent
        if (!speakerElement || !textElement) {
            console.warn("Un sous-titre est mal formé :", subtitle);
            return;
        }

        const currentSpeaker = speakerElement.textContent.trim();
        let text = textElement.textContent.trim();

        // Nettoyer les balises Speaker et Timestamp dans le texte
        text = text.replace(/\[\d{1,2}:\d{2}(:\d{2})?\]/g, '').replace(/\[Speaker \d+\]/g, '').trim();

        if (currentGroup && currentGroup.speaker === currentSpeaker) {
            // Ajouter le texte au groupe actuel
            currentGroup.text += ` ${text}`;
            currentGroup.end = end; // Mettre à jour l'heure de fin
        } else {
            // Sauvegarder le groupe précédent
            if (currentGroup) {
                groupedSubtitles.push(currentGroup);
            }

            // Démarrer un nouveau groupe
            currentGroup = {
                speaker: currentSpeaker,
                start: start,
                end: end,
                text: text,
            };
        }
    });

    // Ajouter le dernier groupe
    if (currentGroup) {
        groupedSubtitles.push(currentGroup);
    }

    // Mettre à jour le DOM
    const editor = document.getElementById('editor');
    editor.innerHTML = ''; // Réinitialiser l'éditeur

    groupedSubtitles.forEach((group) => {
        const subtitleElement = document.createElement('p');
        subtitleElement.classList.add('subtitle');
        subtitleElement.setAttribute('data-start', group.start);
        subtitleElement.setAttribute('data-end', group.end);

        subtitleElement.innerHTML = `
            [<span class="timestamp" onclick="seekTo(${group.start})">${formatTime(group.start)}</span>]
            [<span class="speaker">${group.speaker}</span>]
            <span class="text">${group.text}</span>
        `;
        editor.appendChild(subtitleElement);
    });

    alert("Regroupement et nettoyage effectués !");
}

// Fonction utilitaire pour formater le temps en MM:SS
function formatTime(seconds) {
    const minutes = Math.floor(seconds / 60);
    const secs = Math.floor(seconds % 60);
    return `${minutes}:${secs < 10 ? '0' : ''}${secs}`;
}


updateSpeedDisplay();
audio.addEventListener('timeupdate', updateCustomTimeInput);
customTimeInput.addEventListener('input', jumpToCustomTime);
