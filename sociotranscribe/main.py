# coding: utf-8
#############################################################################
# Copyright Justin Missaghieh-Poncet, 2023-2024
# D'après les scripts de :  https://www.css.cnrs.fr/whisper-pour-retranscrire-des-entretiens/ https://gist.github.com/ThioJoe/e84c4e649857bf0a290b49a2caa670f7
#
# This software is a computer program whose purpose is to [describe
# functionalities and technical features of your software].
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#############################################################################

import warnings
warnings.filterwarnings("ignore", message=".*The 'nopython' keyword.*")
import sys
import os
import configparser
# import cProfile
# import platform
from faster_whisper import WhisperModel
import time
import json
import argparse
import ffmpeg
from datetime import datetime
from pyannote.audio import Pipeline
from pyannote.core import Segment, Annotation, Timeline
import csv
from pathlib import Path
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

VERSION = "1.0.2"
def load_config():
    config = configparser.ConfigParser()
    if not os.path.isfile('config.ini'):
        config['DEFAULT'] = {
            'FORMAT': 'plain-timecode',
            'MODELE': 'large-v3',
            'DIARIZATION': '',
            'SPEAKERS': '2',
            'EMAIL_EMAIL': '',
            'EMAIL_PASSWORD':'',
            'EMAIL_SMTP_SERVER': '',
            'EMAIL_SMTP_PORT':587
        }
        with open('config.ini', 'w') as configfile:
            config.write(configfile)
    config.read('config.ini')
    return {
        'DEFAULT_MODEL': config.get('DEFAULT', 'MODELE', fallback='large-v3'),
        'DEFAULT_FORMAT': config.get('DEFAULT', 'FORMAT', fallback='plain-timecode'),
        'DEFAULT_DIARIZATION': config.get('DEFAULT', 'DIARIZATION', fallback=''),
        'DEFAULT_SPEAKERS': config.getint('DEFAULT', 'SPEAKERS', fallback=2),
        'DEFAULT_EMAIL_EMAIL': config.get('DEFAULT', 'EMAIL_EMAIL', fallback=''),
        'DEFAULT_EMAIL_PASSWORD': config.get('DEFAULT', 'EMAIL_PASSWORD', fallback=''),
        'DEFAULT_EMAIL_SMTP_SERVER': config.get('DEFAULT', 'EMAIL_SMTP_SERVER', fallback=''),
        'DEFAULT_EMAIL_SMTP_PORT': config.get('DEFAULT', 'EMAIL_SMTP_PORT', fallback=587)
    }
config = load_config()

#if platform.system() == 'Windows':
#elif platform.system() == "Linux":
#elif platform.system() == "Darwin":
#    if platform.machine() == "arm64":
#    elif platform.machine() == "x86_64":
#        cpu_count = int(check_output(["sysctl", "-n", "hw.logicalcpu_max"]))
#    else:
#        raise Exception("Unsupported mac")
#else:
#    raise Exception('Platform not supported yet.')
  
help_model = "Modele name: tiny, base, small, medium, large-v3 (default)"
help_format = "Your output format: \n\
                        all (all format, in default), \n\
                        plain (Plain text), \n\
                        plain-timecode (with timecode), \n\
                        raw (without timecode or speaker), \n\
                        html (html format), \n\
                        webvtt (WebVTT), \n\
                        srt (SRT), \n\
                        json (with -d for Gecko), \n\
                        ort (with -d for OTranscribe), \n\
                        nvivo (with -d for NVivo), \n\
                        elan (with -d for ELAN), \n\
                        sonal (with -d, for Sonal)"
help_diarization = "Your token in huggingface.co for pyannote/speaker-diarization-3.1 model"
help_speakers = "the number of speakers in the conversation (default 2)"

           
def convertir(seconds):
    return f"{int(seconds // 3600):02}:{int((seconds % 3600) // 60):02}:{int(seconds % 60):02}"

def not_wav(input_file: str) -> bool:
    if Path(input_file).suffix.lower() == '.wav':
        print(f"{input_file} is already a .wav file, conversion not necessary.")
        return False
    return True

def convert_file(input_file: str) -> str:
    base, _ = os.path.splitext(input_file)
    output_file = base + '.wav'
    if not input_file.lower().endswith('.wav'):
        ffmpeg.input(input_file).output(output_file).run(overwrite_output=True)
        print(f"The conversion from {input_file} to {output_file} has been successfully completed.")
    else:
        print(f"{input_file} is already a .wav file, conversion not necessary.")
        return input_file
    return output_file


### START INSPIRED BY https://github.com/yinruiqing/pyannote-whisper ####
def merge_cache(text_cache):
    sentence = ''.join([item[-1] for item in text_cache])
    spk = text_cache[0][1]
    start = text_cache[0][0].start
    end = text_cache[-1][0].end
    return Segment(start, end), spk, sentence

PUNC_SENT_END = ['.', '?', '!']

def merge_sentence_old(spk_text):
    merged_spk_text = []
    pre_spk = None
    text_cache = []
    for seg, spk, text in spk_text:
        if spk != pre_spk and pre_spk is not None and len(text_cache) > 0:
            merged_spk_text.append(merge_cache(text_cache))
            text_cache = [(seg, spk, text)]
            pre_spk = spk

        elif text and len(text) > 0 and text[-1] in PUNC_SENT_END:
            text_cache.append((seg, spk, text))
            merged_spk_text.append(merge_cache(text_cache))
            text_cache = []
            pre_spk = spk
        else:
            text_cache.append((seg, spk, text))
            pre_spk = spk
    if len(text_cache) > 0:
        merged_spk_text.append(merge_cache(text_cache))
    return merged_spk_text


def merge_sentence(spk_text):
    merged_spk_text = []
    text_cache = []
    prev_speaker = None
    
    for seg, spk, text in spk_text:
        if spk != prev_speaker and text_cache:
            merged_text = " ".join([t for _, _, t in text_cache])
            start = text_cache[0][0].start
            end = text_cache[-1][0].end
            merged_spk_text.append((Segment(start, end), prev_speaker, merged_text))
            text_cache = []
        text_cache.append((seg, spk, text))
        prev_speaker = spk
    
    if text_cache:
        merged_text = " ".join([t for _, _, t in text_cache])
        start = text_cache[0][0].start
        end = text_cache[-1][0].end
        merged_spk_text.append((Segment(start, end), prev_speaker, merged_text))
    
    return merged_spk_text

### END INSPIRED BY https://github.com/yinruiqing/pyannote-whisper ####
def determinate_output_filename(output_txt, format_type, diarization=False):
    extension = {
        "plain": "_plain.txt",
        "plain-timecode": "_plain-timecode.txt",
        "raw" : "_raw.txt",
        "html": ".html",
        "nvivo": "_nvivo.txt",
        "elan": "_elan.txt",
        "sonal": "_sonal.txt",
        "webvtt": ".vtt",
        "srt": ".srt",
        "otr": ".otr",
        "json": ".json"
    }[format_type]
    if diarization:
        return f"{output_txt}_diarize{extension}"
    else:
        return f"{output_txt}{extension}"

def export_plaintext_old(output_txt, segments, diarization=False, timecode=True):
    if diarization:
        with open(output_txt + "_diarize_plain.txt", 'w', encoding='utf-8') as f:         
            for seg, spk, sent in segments:
                start_segment = convertir(seg.start)
                end_segment = convertir(seg.end)
                if timecode:
                    f.write(f"[{start_segment} - {end_segment}] [{spk}] {sent}\n")
                else:
                     f.write(f"[{spk}] {sent}\n")
    else:
        with open(output_txt + "_plain.txt", 'w', encoding='utf-8') as f:
            for segment in segments:
                start_segment = convertir(segment.start)
                end_segment = convertir(segment.end)
                clean_text = segment.text
                
                if timecode:
                    f.write(f"[{start_segment} - {end_segment}] {clean_text}\n")
                else:
                    f.write(f"{clean_text}\n")

def export_plaintext(output_txt, segments, diarization=False, format_type="plain-timecode"):
    if diarization:
         with open(determinate_output_filename(output_txt, format_type, True), 'w', encoding='utf-8') as f:
             for seg, spk, sent in segments:
                 start_segment = convertir(seg.start)
                 end_segment = convertir(seg.end)
                 if format_type == "plain":
                     f.write(f"[{spk}] {sent}\n")
                 elif format_type == "plain-timecode":
                     f.write(f"[{start_segment} - {end_segment}] [{spk}] {sent}\n")
                 elif format_type == "raw":
                     f.write(f"{sent}\n")
                 elif format_type == "html":
                     f.write(f"<p><b>{spk} : </b> {sent}</p>\n")
                 elif format_type == "nvivo":
                     f.write(f"{start_segment}\t{spk}\t{sent}\n")
                 elif format_type == "elan":
                     f.write(f"{start_segment}\t{end_segment}\t\t{spk}\t{sent}\n")
                 elif format_type == "sonal":
                     f.write(f"[{start_segment}] [{spk}] {sent}\n")
    else:
         with open(determinate_output_filename(output_txt, format_type), 'w', encoding='utf-8') as f:
             for segment in segments:
                 start_segment = convertir(segment.start)
                 end_segment = convertir(segment.end)
                 clean_text = segment.text
                 if format_type == "plain":
                     f.write(f"[SPEAKER] {clean_text}\n")
                 elif format_type == "plain-timecode":
                     f.write(f"[{start_segment} - {end_segment}] {clean_text}\n")
                 elif format_type == "raw":
                     f.write(f"{clean_text}\n")
                 elif format_type == "html":
                     f.write(f"<p>{clean_text}</p>\n")
                 elif format_type == "nvivo":
                     f.write(f"{start_segment}\tSPEAKER\t{clean_text}\n")
                 elif format_type == "elan":
                     f.write(f"{start_segment}\t{end_segment}\t\tSPEAKER\t{clean_text}\n")
                 elif format_type == "sonal":
                     f.write(f"[{start_segment}] [SPEAKER] {clean_text}\n")

def export_subtitles(output_txt, segments, format_type, diarization=False):
    with open(determinate_output_filename(output_txt, format_type, diarization), 'w', encoding='utf-8') as f:
        # Ajout de l'en-tête pour le format WebVTT
        if format_type == "webvtt":
            f.write("WEBVTT\n\n")
        
        number_subtitle = 1
        for segment in segments:
            if isinstance(segment, tuple):  # Cas avec diarisation
                seg, spk, text = segment
                start_segment = convertir(seg.start)
                end_segment = convertir(seg.end)
                clean_text = text
            else:  # Cas sans diarisation (objet avec .start et .end)
                start_segment = convertir(segment.start)
                end_segment = convertir(segment.end)
                clean_text = segment.text
            if format_type == "webvtt":
                f.write(f"{number_subtitle}\n")
                f.write(f"{start_segment}.000 --> {end_segment}.000\n")
            elif format_type == "srt":
                f.write(f"{number_subtitle}\n")
                f.write(f"{start_segment},000 --> {end_segment},000\n")
            if diarization:
                f.write(f"{spk}: {clean_text}\n\n")
            else:
                f.write(f"{clean_text}\n\n")
            
            number_subtitle += 1

            
def export_otr(input_audio_path, output_txt, segments):
    otrjson = {"text":"", "media":input_audio_path, "media-time":0}
    html=[f"""<p><span class=\"timestamp\" data-timestamp=\"{seg.start}\">{convertir(seg.start)}</span> [{spk}] {sent}</p>""" for seg, spk, sent in segments]
    otrjson["text"]= "".join(html)
    with open(determinate_output_filename(output_txt, 'otr'), 'w', encoding='utf-8') as f:
        json.dump(otrjson, f, ensure_ascii=False)

def export_json(output_txt, segments):
    monologues = {"schemaVersion":"2.0", "monologues":[]}
    for seg, spk, sent in segments:
        monologue = { "speaker": {"id": spk, "name": spk}, "start": 0, "end": 0, "terms": []}
        monologue["start"] = seg.start
        monologue["end"] = seg.end
        monologue["terms"] = [{"start": seg.start, "end": seg.end, "text":sent}]
        monologues["monologues"].append(monologue)
    with open(determinate_output_filename(output_txt, 'json'), 'w', encoding='utf-8') as f:
        json.dump(monologues, f, ensure_ascii=False)

def export_output(output_txt, final_result, diarization_option, format_type):
    export_methods = {
        "plain-timecode": lambda: export_plaintext(output_txt, final_result, diarization=diarization_option, format_type='plain-timecode'),
        "plain": lambda: export_plaintext(output_txt, final_result, diarization=diarization_option, format_type='plain'),
        "raw": lambda: export_plaintext(output_txt, final_result, diarization=diarization_option, format_type='raw'),
        "html": lambda: export_plaintext(output_txt, final_result, diarization=diarization_option, format_type='html'),
        "nvivo": lambda: export_plaintext(output_txt, final_result, diarization=diarization_option, format_type='nvivo'),
        "elan": lambda: export_plaintext(output_txt, final_result, diarization=diarization_option, format_type='elan'),
        "sonal": lambda: export_plaintext(output_txt, final_result, diarization=diarization_option, format_type='sonal'),
        "json": lambda: export_json(output_txt, final_result),
        "otr": lambda: export_otr(input_audio, output_txt, final_result),
        "webvtt": lambda: export_subtitles(output_txt, final_result, "webvtt", diarization=diarization_option),
        "srt": lambda: export_subtitles(output_txt, final_result, "srt", diarization=diarization_option)
        # "all": lambda: export_all(output_txt, final_result, diarization_option)
    }
    export_methods.get(format_type, export_methods[config['DEFAULT_FORMAT']])()
    
def transcribe(input_audio, modele, output_format, diarization, speakers):
    start = time.time()
    if not_wav(input_audio):
        audio_file = convert_file(input_audio)
    else:
        audio_file = input_audio
    
    model = WhisperModel(modele, device="auto", compute_type="auto")
    #  segments, info = model.transcribe(audio_file)
    segments, info = model.transcribe(audio_file, beam_size=1, temperature=0, vad_filter=True, vad_parameters=dict(min_silence_duration_ms=200))
    print("Detected language '%s' with probability %f" % (info.language, info.language_probability))
    #if len(diarization) > 1 and speakers > 1:
    if speakers > 1:
        # START Diarization
        diarization_option = True
        pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization-3.1", use_auth_token=diarization)
        diarization_result = pipeline(audio_file, num_speakers=speakers)
        timestamp_texts = []
        for segment in segments:
            timestamp_texts.append((Segment(segment.start, segment.end), segment.text))

        spk_text = []
        for seg, text in timestamp_texts:
              spk = diarization_result.crop(seg).argmax()
              spk_text.append((seg, spk, text))
        final_result = merge_sentence(spk_text)
    else:
        diarization_option = False
        final_result = segments
    # END Diarization
    output_file = determinate_output_filename(input_audio, output_format, diarization_option)
    export_output(input_audio, final_result, diarization_option, output_format)

    end = time.time()
    elapsed = str(round(float(end - start) / 60, 2))
    return {
        'input_file': input_audio,
        'output_file': output_file,
        'model': modele,
        'output_format': output_format,
        'speakers': speakers,
        'date_processed': datetime.fromtimestamp(start).isoformat().split('.')[0],
        'duration_traitement': str(elapsed)
    }

def log_transcribe(filename, outputfile, model, output_format, speakers, date_submission, date_processed, duration_traitement, status):
    log_entries = [
        f"Input File\t: {filename}",
        f"Output File\t: {outputfile}",
        f"Model\t: {model}",
        f"Format\t: {output_format}",
        f"Number of speakers\t: {speakers}",
        f"Date of submission\t: {date_submission}",
        f"Date processed\t: {date_processed}",
        f"Duration of treatment\t: {duration_traitement}",
        f"Status\t: {status}"
    ]
    
    with open(outputfile + ".log", 'w') as fichier:
        fichier.write("\n".join(log_entries) + "\n")
    return "\n".join(log_entries) + "\n"


####### Fonctions queue #########
def send_email(receiver_email, subject, body):
    try:
        msg = MIMEMultipart()
        msg['From'] = config['DEFAULT_EMAIL_EMAIL']
        msg['To'] = receiver_email
        msg['Subject'] = subject
        msg.attach(MIMEText(body, 'plain'))
        with smtplib.SMTP(config['DEFAULT_EMAIL_SMTP_SERVER'], config['EMAIL_SMTP_PORT']) as server:
            server.starttls()  # Démarrer la connexion sécurisée
            server.login(config['DEFAULT_EMAIL_EMAIL'], config['DEFAULT_EMAIL_PASSWORD'])
            server.sendmail(config['DEFAULT_EMAIL_EMAIL'], receiver_email, msg.as_string())

        print('E-mail envoyé avec succès.')
        return True
    except Exception as e:
        print(f'Une erreur est survenue : {e}')
        return False
        
def add_task(input_audio, modele, output_format, speakers, email='', status='pending'):
    with open('tasks.csv', mode='a', newline='') as file:
        date_submission = datetime.now().isoformat().split('.')[0]
        writer = csv.writer(file)
        if speakers > 1:
            diarization_option = True
        else:
            diarization_option = False
        export_file = determinate_output_filename(input_audio, output_format, diarization_option)
        writer.writerow([input_audio, export_file, export_file+'.log', modele, output_format, speakers,date_submission, '', '', email, status])
    print(f"Added task: input_file={input_audio}, output_file={export_file}, modele={modele}, format={output_format}, speakers={speakers}, status={status}")

def process_tasks():
# 0 = input audio ; 1 = fichier de sortie ; 2 = modele ; 3 = format ; 4 = speaker ; 5 = date_submission, 6 = date_processed ; 7 = duration_traitement ; 8 = email, 9 = Statut
    if not os.path.isfile('tasks.csv'):
        print("The tasks file ‘tasks.csv’ does not exist.")
        return
    with open('tasks.csv', mode='r') as infile:
        reader = csv.reader(infile)
        tasks = list(reader)
    tasks_processed = False
    for row in tasks:
        if row[10] == 'pending':
            input_audio = row[0]
            modele = row[3]
            output_format = row[4]
            speakers = int(row[5])

            try:
                row[7] = datetime.now().isoformat().split('.')[0]
                info_transcribe = transcribe(input_audio, modele, output_format, config['DEFAULT_DIARIZATION'], speakers)
                row[8] = info_transcribe['duration_traitement']
                row[10] = 'completed'
                logtxt = log_transcribe(info_transcribe['input_file'], info_transcribe['output_file'], info_transcribe['model'], info_transcribe['output_format'], info_transcribe['speakers'], row[5], info_transcribe['date_processed'], info_transcribe['duration_traitement'], 'completed')
            except Exception as e:
                print(f"Error during task execution {input_audio}: {e}")
                row[10] = 'failed'
            tasks_processed = True
            if len(config['DEFAULT_EMAIL_EMAIL']) > 1:
                send_email(row[9], 'Transcribe process completed', logtxt)
        
    if tasks_processed:
        with open('tasks.csv', mode='w', newline='') as outfile:
            writer = csv.writer(outfile)
            writer.writerows(tasks)
        
    else:
        print("No pending tasks.")
##################################
def run_cli():
    parser = argparse.ArgumentParser(description="Transcribe audio files with Whisper")
    parser.add_argument('--version', action='version', version=f'%(prog)s {VERSION}')
    parser.add_argument("-f", "--file", dest="FILE", help="Audio file name")
    parser.add_argument("-F", "--format", dest="FORMAT", default=config['DEFAULT_FORMAT'], help=help_format)
    parser.add_argument("-m", "--modele", dest="MODELE", default=config['DEFAULT_MODEL'], help=help_model)
    parser.add_argument("-d", "--diarization", dest="DIARIZATION", default=config['DEFAULT_DIARIZATION'], help=help_diarization)
    parser.add_argument("-s", "--speakers", dest="SPEAKERS", type=int, default=config['DEFAULT_SPEAKERS'], help=help_speakers)
    parser.add_argument("--add-queue", dest="ADDQUEUE", action='store_true', help="Add task to queue")
    parser.add_argument("--process-queue", dest="PROCESSQUEUE", action='store_true', help="Process tasks from the queue")
    args = parser.parse_args()
    
    if args.ADDQUEUE:
        add_task(args.FILE, args.MODELE, args.FORMAT, args.SPEAKERS)
    elif args.PROCESSQUEUE:
        process_tasks()
    else:
        info_transcribe = transcribe(args.FILE, args.MODELE, args.FORMAT, args.DIARIZATION, args.SPEAKERS)
        print("Transcript file in "+info_transcribe['duration_traitement']+" minute(s)")
        print("Exported file:" +info_transcribe['output_file'])
        log_transcribe(info_transcribe['input_file'], info_transcribe['output_file'], info_transcribe['model'], info_transcribe['output_format'], info_transcribe['speakers'], datetime.now().isoformat().split('.')[0], info_transcribe['date_processed'], info_transcribe['duration_traitement'], 'completed')


def run_gui():
    import tkinter as tk
    from tkinter import filedialog, messagebox, ttk
    from idlelib.tooltip import Hovertip
    import webbrowser
    def callback():
        webbrowser.open_new("https://framagit.org/justinmponcet/whisper")

    def browse_file():
        filename = filedialog.askopenfilename(filetypes=[("Audio Files", "*.3dostr *.4xm *.aa *.aac *.ac3 *.acm *.act *.adf *.adp *.ads *.adx *.aea *.afc *.aiff *.aix *.alaw *.alias_pix *.alp *.amr *.amrnb *.amrwb *.anm *.apc *.ape *.apm *.apng *.aptx *.aptx_hd *.aqtitle *.argo_asf *.asf *.asf_o *.ass *.ast *.au *.av1 *.avi *.avr *.avs *.avs2 *.bethsoftvid *.bfi *.bfstm *.bin *.bink *.bit *.bmp_pipe *.bmv *.boa *.brender_pix *.brstm *.c93 *.caf *.cavsvideo *.cdg *.cdxl *.cine *.codec2 *.codec2raw *.concat *.data *.daud *.dcstr *.dds_pipe *.derf *.dfa *.dhav *.dirac *.dnxhd *.dpx_pipe *.dsf *.dsicin *.dss *.dts *.dtshd *.dv *.dvbsub *.dvbtxt *.dxa *.ea *.ea_cdata *.eac3 *.epaf *.exr_pipe *.f32be *.f32le *.f64be *.f64le *.fbdev *.ffmetadata *.film_cpk *.filmstrip *.fits *.flac *.flic *.flv *.frm *.fsb *.fwse *.g722 *.g723_1 *.g726 *.g726le *.g729 *.gdv *.genh *.gif *.gif_pipe *.gsm *.gxf *.h261 *.h263 *.h264 *.hca *.hcom *.hevc *.hls *.hnm *.ico *.idcin *.idf *.iff *.ifv *.ilbc *.image2 *.image2pipe *.ingenient *.ipmovie *.ircam *.iss *.iv8 *.ivf *.ivr *.j2k_pipe *.jacosub *.jpeg_pipe *.jpegls_pipe *.jv *.kux *.kvag *.lavfi *.live_flv *.lmlm4 *.loas *.lrc *.lvf *.lxf *.m4v *.matroska *.webm *.mgsts *.microdvd *.mjpeg *.mjpeg_2000 *.mlp *.mlv *.mm *.mmf *.mov *.mp4 *.m4a *.3gp *.3g2 *.mj2 *.mkv *.mp3 *.mpc *.mpc8 *.mpeg *.mpegts *.mpegtsraw *.mpegvideo *.mpjpeg *.mpl2 *.mpsub *.msf *.msnwctcp *.mtaf *.mtv *.mulaw *.musx *.mv *.mvi *.mxf *.mxg *.nc *.nistsphere *.nsp *.nsv *.nut *.nuv *.ogg *.oma *.oss *.paf *.pam_pipe *.pbm_pipe *.pcx_pipe *.pgm_pipe *.pgmyuv_pipe *.pictor_pipe *.pjs *.pmp *.png_pipe *.pp_bnk *.ppm_pipe *.psd_pipe *.psxstr *.pva *.pvf *.qcp *.qdraw_pipe *.r3d *.rawvideo *.realtext *.redspark *.rl2 *.rm *.roq *.rpl *.rsd *.rso *.rtp *.rtsp *.s16be *.s16le *.s24be *.s24le *.s32be *.s32le *.s337m *.s8 *.sami *.sap *.sbc *.sbg *.scc *.sdp *.sdr2 *.sds *.sdx *.ser *.sgi_pipe *.shn *.siff *.sln *.smjpeg *.smk *.smush *.sol *.sox *.spdif *.srt *.stl *.subviewer *.subviewer1 *.sunrast_pipe *.sup *.svag *.svg_pipe *.swf *.tak *.tedcaptions *.thp *.tiertexseq *.tiff_pipe *.tmv *.truehd *.tta *.tty *.txd *.ty *.u16be *.u16le *.u24be *.u24le *.u32be *.u32le *.u8 *.v210 *.v210x *.vag *.vc1 *.vc1test *.vidc *.video4linux2 *.v4l2 *.vividas *.vivo *.vmd *.vobsub *.voc *.vpk *.vplayer *.vqf *.w64 *.wav *.wc3movie *.webm_dash_manifest *.webp_pipe *.webvtt *.wsaud *.wsd *.wsvqa *.wtv *.wv *.wve *.xa *.xbin *.xmv *.xpm_pipe *.xvag *.xwd_pipe *.xwma *.yop *.yuv4mpegpipe *.opus")])
        if filename:
            file_entry.delete(0, tk.END)
            file_entry.insert(0, filename)

    def start_transcription():
        start_button.config(text="Transcription in progress...", state=tk.DISABLED)
        input_file = file_entry.get()
        modele = model_var.get()
        output_format = format_var.get()
        # diarization = diarization_entry.get()
        speakers = int(speakers_spinbox.get())
        if speakers > 1:
            diarization = True
        else:
            diarization = False
        if not input_file:
            messagebox.showerror("Error", "Please select an audio file.")
            start_button.config(text="Start Transcription", state=tk.NORMAL)
            return
            
        try:
            if checkbox_queue.get():
                add_task(input_file, modele, output_format, speakers)
                messagebox.showinfo("Success", f"Transcription for {input_file} added to queue")
            else:
                info_transcribe = transcribe(input_file, modele, output_format, config['DEFAULT_DIARIZATION'], speakers)
                text_box = "Transcription completed for "+info_transcribe['input_file'] + " "+info_transcribe['output_file'] + " in "+info_transcribe['duration_traitement']+" minute(s)"
                log_transcribe(info_transcribe['input_file'], info_transcribe['output_file'], info_transcribe['model'], info_transcribe['output_format'], info_transcribe['speakers'], datetime.now().isoformat().split('.')[0], info_transcribe['date_processed'], info_transcribe['duration_traitement'], 'completed')
                messagebox.showinfo("Success", text_box)            
        except Exception as e:
            messagebox.showerror("Error", str(e))
        finally:
            start_button.config(text="Start Transcription", state=tk.NORMAL)

    root = tk.Tk()
    root.title(f"SocioTranscribe {VERSION} - Interview transcript")
    #root.config(padx=100, pady=100)
    
    tk.Label(root, text="Audio File:").grid(row=0, column=0, padx=5, pady=5, sticky="e")
    file_entry = tk.Entry(root, width=50)
    file_entry.grid(row=0, column=1, padx=5, pady=5)
    tk.Button(root, text="Browse", command=browse_file).grid(row=0, column=2, padx=5, pady=5)


    tk.Label(root, text="Model:").grid(row=1, column=0, padx=5, pady=5, sticky="e")
    model_var = tk.StringVar(value=config['DEFAULT_MODEL'])
    model_options = ["tiny", "base", "small", "medium", "large-v3"]
    model_menu = tk.OptionMenu(root, model_var, *model_options)
    model_menu.grid(row=1, column=1, padx=5, pady=5, sticky="w")
    Hovertip(model_menu, help_model, hover_delay=200)

    tk.Label(root, text="Output Format:").grid(row=2, column=0, padx=5, pady=5, sticky="e")
    format_var = tk.StringVar(value=config['DEFAULT_FORMAT'])
    format_options = ["all", "plain-timecode", "plain", "raw", "html", "webvtt", "srt", "json", "otr", "nvivo", "elan", "sonal"]
    format_menu = tk.OptionMenu(root, format_var, *format_options)
    format_menu.grid(row=2, column=1, padx=5, pady=5, sticky="w")
    Hovertip(format_menu, help_format, hover_delay=200)

    tk.Label(root, text="Number of Speakers:").grid(row=4, column=0, padx=5, pady=5, sticky="e")
    speakers_spinbox = tk.Spinbox(root, from_=1, to=10, width=5)
    speakers_spinbox.grid(row=4, column=1, padx=5, pady=5, sticky="w")
    speakers_spinbox.delete(0, tk.END)
    speakers_spinbox.insert(0, config['DEFAULT_SPEAKERS'])
    Hovertip(speakers_spinbox, help_speakers, hover_delay=200)
    
    checkbox_queue = tk.BooleanVar()
    checkbox = tk.Checkbutton(root, text="Add to queue", variable=checkbox_queue)
    checkbox.grid(row=5, column=0,  padx=5, pady=5)

    start_button = tk.Button(root, text="Start Transcription", command=start_transcription)
    start_button.grid(row=6, column=0, columnspan=3, padx=5, pady=10)

    website_button = tk.Button(text="Visit website", command=callback)
    website_button.grid(column=0, row=6)


    root.mainloop()

from flask import Flask, render_template, request, redirect, send_from_directory
from werkzeug.utils import secure_filename

app = Flask(__name__, static_folder="static")
app.config['UPLOAD_FOLDER'] = os.path.join(app.static_folder, 'uploads')
os.makedirs(app.config['UPLOAD_FOLDER'], exist_ok=True)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in {'wav', 'mp3', 'mp4', 'm4a', 'aac'}

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            return redirect(request.url)
        if file and allowed_file(file.filename):
            original_filename = secure_filename(file.filename)
            timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
            filename = f"{timestamp}_{original_filename}"
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filepath)

            email = request.form.get("email", '')
            modele = request.form.get("modele", config['DEFAULT_MODEL'])
            output_format = request.form.get("format", config['DEFAULT_FORMAT'])
            speakers = int(request.form.get("speakers", config['DEFAULT_SPEAKERS']))
            add_task(filepath, modele, output_format, speakers, email)

    data = []
    if os.path.exists('tasks.csv'):
        with open('tasks.csv', newline='') as file:
            reader = csv.reader(file)
            for row in reader:
                base_url = request.host_url
                row[0] = (f"{base_url}static/uploads/{os.path.basename(row[0])}", os.path.basename(row[0]))
                row[1] = (f"{base_url}static/uploads/{os.path.basename(row[1])}", os.path.basename(row[1]))
                row[2] = (f"{base_url}static/uploads/{os.path.basename(row[2])}", os.path.basename(row[2]))
                data.append(row)
    return render_template(
        'index.html',
        data=data,
        DEFAULT_MODEL=config['DEFAULT_MODEL'],
        DEFAULT_FORMAT=config['DEFAULT_FORMAT'],
        DEFAULT_DIARIZATION=config['DEFAULT_DIARIZATION'],
        DEFAULT_SPEAKERS=config['DEFAULT_SPEAKERS'],
        version=VERSION,
        base_url=request.host_url
    )


@app.route('/uploads/<filename>', methods=['GET'])
def get_file(filename):
    uploads_dir = os.path.join(app.static_folder, 'uploads')
    try:
        return send_from_directory(uploads_dir, filename)
    except FileNotFoundError:
        return {"error": "File not found"}, 404


# app.run(debug=True)

@app.route('/processqueue')
def processqueue():
    try:
        process_tasks()  # Call your processing function
        return {"status": "Queue processed successfully"}, 200  # Return a JSON response
    except Exception as e:
        app.logger.error(f"Error in processqueue: {e}")
        return {"error": "Failed to process tasks"}, 500  # Return an error response

@app.route('/correction')
def correction():
    return render_template('correction.html')
    
if __name__ == "__main__":
    if len(sys.argv) > 1:
        mode = sys.argv[1]
        if mode == "--gui":
            run_gui()
        else:
            run_cli()
    else:
        run_cli()

