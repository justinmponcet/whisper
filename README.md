## SocioTranscribe


SocioTranscribe est logiciel pour retranscrire des entretiens et les exporter dans de nombreux formats. C'est un script python se utilisant sur Whisper/Faster-Whisper et PyAnnote. Il y a également une interface de correction permettant de corriger le fichier retranscrit.


## Installation

1. Si cela n’est déjà fait, installez la dernière version de Python : Rendez-vous sur le [site officiel de Python](https://www.python.org/) et téléchargez la version la plus récente qui correspond à votre système d'exploitation (Windows, MacOS, ou Linux). Sur Windows (et peut-être MacOS), avant de cliquer sur « Install Now », assurez-vous de cocher la case « Add Python to PATH » en bas de la fenêtre de l’installateur. 

2. Téléchargez la [dernière version](https://framagit.org/justinmponcet/whisper/-/releases) de SocioTranscribe et extrayez son contenu.

3. Ouvrez une fenêtre de terminal ou d'invite de commande (en fonction de votre ordinateur), puis suivez les instructions ci-dessous dans le dossier que vous venez de décompresser (nota : parfois il faudra remplacer  `python` par `python3` selon la version installée) :

Sous Linux ou MacOS :

```
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

```

Sous Windows :
```
python -m venv .venv
.venv\Scripts\activate
pip install -r requirements.txt
```

Si vous ne souhaitez pas exécuter en ligne de commande, double cliquez le script d’installation (setup.bat pour Windows, setup.sh pour Linux/MacOS).

## Utilisation
Vous pouvez préremplir les arguments (ou l’interface clic-bouton) en modifiant le fichier config.ini (généré au premier lancement).

### En ligne de commande
Pour lancer le script : ``python sociotranscribe/main.py``

Arguments :

- -f ou --file : le fichier à convertir : le format du fichier doit être obligatoirement en .wav
- -m ou --modele : le modèle Whisper
- -F ou --format : Votre format de sortie  :
	- all (ensemble des formats, par défaut)
	- plain-timecode (Texte brut avec le timecode)
	- plain (sans timecode)
	- raw (sans timecode ni locuteur)
	- html (format HTML)
	- webvtt (WebVTT)
	- srt (SRT)
	- json (avec -d pour [gecko](https://github.com/gong-io/gecko))
	- ort (avec -d pour [Otranscribe](https://github.com/oTranscribe/oTranscribe))
	- nvivo (avec -d pour  NVivo)
    - elan (avec -d pour  ELAN)
    - sonal (avec -d, pour Sonal)
- -d ou --diarization : Activer la diarization, c’est-à-dire la reconnaissance d’interlocuteurs. En entrée, le token pour huggingface. Nécessite d’accepter les clauses de [pyannote/speaker-diarization](https://huggingface.co/pyannote/speaker-diarization-3.0)
- -s ou -speakers : le nombre d’interlocuteurs pour la diarization. Par défaut, 2.
- --add-queue : Ajouter une tâche à une liste d’attente
- --process-queue : Exécuter la liste d’attente

### En interface graphique
Pour lancer le script avec une interface graphique, vous lancez avec l’argument `gui` : `python sociotranscribe/main.py --gui`.

![Aperçu GUI](doc/gui.png)

### Sur un serveur web
Vous pouvez également installer sur un serveur (ou en local) et lancer l’application (`gunicorn sociotranscribe.main:app -w 4`). Les utilisateurs peuvent se rendre sur le site et télécharger les fichiers. L’application rajoute les fichiers à retranscrire à la liste d’attente, à vous de faire un cron pour exécuter la liste d’attente (`python sociotranscribe/main.py --process-queue` ou une requête web sur `processqueue`).

Sous Windows Gunicorn ne fonctionne pas. Il faut installer waitress : `pip install waitress` et lancer de la manière suivante : `waitress-serve --listen=127.0.0.1:5000 sociotranscribe.main:app`

![Aperçu interface web](doc/web1.png)

# Docker (Work in Progress)

Pour transformer en docker, [suivre ce tutoriel](https://blog.jaaj.dev/2023/02/10/Comment-dockeriser-une-application-flask.html).

```
docker build -t sociotranscribe .
```
Pour lancer en tant que docker :
```
docker run -p 5000:8000 sociotranscribe
```
# Remerciements

Merci à :
- Matthieu Guionnet pour la correction d'un bug et le support de Gecko et Otranscribe
- Béatrice Mazoyer pour avoir l'option sans timecode pour le format brut.
- L’équipe du GRICAD pour le formatage pour les logiciels de CAQDAS (voir [diaporama](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-rech/23-outils-gricad-larac.pdf), p6).

Point technique :

- Pour retranscrire 30 minutes d'audio, avec le modèle large et avec mon ordinateur avec un processeur i5 et 16GO de RAM, je mets 1 heure.

# On parle de SocioTranscribe

- Yacine. Automatiser et autonomiser la retranscription d’entretien – CSS @ IPP. 7 mars 2024, https://www.css.cnrs.fr/fr/whisper-pour-retranscrire-des-entretiens2/.


# Logiciels similaires

- [No Scribe](https://github.com/kaixxx/noScribe)
- [Vibe](https://github.com/thewh1teagle/vibe)
- [DSNote](https://github.com/mkiol/dsnote)

