# Changelog
## ???? (202X-XX-XX)
- Interface de correction post-entretien
- Possibilité d’installer via Docker
- Lancer la liste d’attente de traitement en requête GET pour le web. 

## 1.0.2 (2024-12-02)
- Possibilité de diarize les SRT et Webvtt

## 1.0.1 (2024-09-03)
- Correction du bug final_result

## [1.0 (2024-08-31)](https://framagit.org/justinmponcet/whisper/-/releases/1.0)
- Renommage du logiciel en SocioTranscribe
- Ajout d’une interface graphique et d’une application web (avec notification par courriel lorsque le fichier est retranscrit).
- Ajout de nouveaux formats d'exports (Otranscribe, JSON, Nvivo, Elan, Sonal, texte brut sans time code, HTML...). Merci à Matthieu Guizonnet et à Béatrice Mazoyer pour leurs contributions !
- Ajout d’un fichier config.ini pour pré-remplir les informations.
- Conversion en .wav lorsque le fichier n’est pas au bon format.
- Amélioration dans la détection et le regroupement des interlocuteurs.
- Ajout d'un système de liste d’attente pour la conversion des fichiers
- Réduction des hallucinations du logiciel

## [0.1 (2024-01-20)](https://framagit.org/justinmponcet/whisper/-/releases/0.1)
Première version, avec utilisation de la diarization et de faster whisper. Possibilité d'exportr en texte brut et en sous-titres
